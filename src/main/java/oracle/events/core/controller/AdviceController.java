package oracle.events.core.controller;

import lombok.extern.slf4j.Slf4j;
import oracle.events.core.exception.EntityNotCreatedException;
import oracle.events.core.exception.EntityNotFoundException;
import oracle.events.core.exception.EntityNotProcessedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestControllerAdvice
@Slf4j
public class AdviceController {

    @ExceptionHandler(value = {EntityNotCreatedException.class})
    void handleNotCreatedException(EntityNotCreatedException e, HttpServletResponse response) throws IOException {
        log.debug("Impossible to create resource. Details {} ", e);
        response.sendError(HttpStatus.NOT_ACCEPTABLE.value());
    }

    @ExceptionHandler(value = {EntityNotFoundException.class})
    void handleNotFoundException(EntityNotFoundException e, HttpServletResponse response) throws IOException {
        log.debug("No found. Deails {}", e);
        response.sendError(HttpStatus.NOT_FOUND.value());
    }

    @ExceptionHandler(value = {EntityNotProcessedException.class})
    void handleNotProcessed(EntityNotProcessedException e, HttpServletResponse response) throws IOException {
        log.debug("No processed. Deails {}", e);
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }

    @ExceptionHandler(value = {Exception.class,})
    void handleException(Exception e, HttpServletResponse response) throws IOException {
        log.error("Unhandled Exception. Details {} ", e);
        response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
    }
}
