package oracle.events.core.controller;

import oracle.events.core.controller.specification.EventBetweenDate;
import oracle.events.core.controller.specification.EventWithDescription;
import oracle.events.core.controller.specification.EventWithTitle;
import oracle.events.core.exception.EntityNotCreatedException;
import oracle.events.core.exception.EntityNotFoundException;
import oracle.events.core.exception.EntityNotProcessedException;
import oracle.events.core.model.Event;
import oracle.events.core.service.EventService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping( value = "/events" )
public class EventController {

    @Autowired
    private EventService eventService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<Event> findAll(){
        return eventService.findAll();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/filter")
    @ResponseStatus(HttpStatus.OK)
    public Page<Event> filter(@RequestParam(required = false) String title, @RequestParam(required=false) String description,
                              @RequestParam(required=false) String initDate, @RequestParam(required=false) String endDate, Pageable pageable){

        LocalDateTime init = null, end = null;

        if(StringUtils.isNotBlank(initDate) && StringUtils.isNotBlank(endDate)){
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

            init = LocalDateTime.parse( initDate.concat(" 00:00"), formatter );
            end = LocalDateTime.parse( endDate.concat(" 23:59"), formatter );
        }

        Specification<Event> spec = Specification.where( new EventWithTitle(title) )
                                    .and( new EventWithDescription(description) )
                                    .and( new EventBetweenDate(init, end) );

        return eventService.findAll(spec, pageable);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Event newEvent(@RequestBody Event event){
        return eventService.saveEvent(event).orElseThrow(EntityNotCreatedException::new);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{eventId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEvent(@PathVariable UUID eventId){
        if(!eventService.deleteEvent(eventId)) throw new EntityNotFoundException();
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/{eventId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Event updateEvent(@PathVariable UUID eventId, @RequestBody Event event){
        Event founded = eventService.findEvent(eventId).orElseThrow(EntityNotFoundException::new);
        return eventService.updateEvent(event, founded).orElseThrow(EntityNotProcessedException::new);
    }

}
