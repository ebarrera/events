package oracle.events.core.controller.specification;

import oracle.events.core.model.Event;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class EventWithTitle implements Specification<Event> {

    private String title;

    public EventWithTitle(String title) {
        this.title = title;
    }

    @Override
    public Predicate toPredicate(Root<Event> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        if(title == null){
            return criteriaBuilder.isTrue(criteriaBuilder.literal(true));
        }

        return criteriaBuilder.like(  criteriaBuilder.lower(root.get("title")),  "%" + this.title.toLowerCase() + "%" );
    }

}
