package oracle.events.core.controller.specification;

import oracle.events.core.model.Event;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;

public class EventBetweenDate implements Specification<Event> {

    private LocalDateTime initDate, endDate;

    public EventBetweenDate(LocalDateTime initDate, LocalDateTime endDate){
        this.initDate = initDate;
        this.endDate = endDate;
    }

    @Override
    public Predicate toPredicate(Root<Event> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        if(initDate == null || endDate == null){
            return criteriaBuilder.isTrue(criteriaBuilder.literal(true));
        }

        return criteriaBuilder.between( root.get("atDate"), initDate, endDate);
    }
}
