package oracle.events.core.controller.specification;

import oracle.events.core.model.Event;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class EventWithDescription implements Specification<Event> {

    private String description;

    public EventWithDescription(String description) {
        this.description = description;
    }

    @Override
    public Predicate toPredicate(Root<Event> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        if(description == null){
            return criteriaBuilder.isTrue(criteriaBuilder.literal(true));
        }

        return criteriaBuilder.like(  criteriaBuilder.lower(root.get("description")),  "%" + this.description.toLowerCase() + "%" );
    }

}
