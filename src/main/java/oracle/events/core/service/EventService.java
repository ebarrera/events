package oracle.events.core.service;

import lombok.extern.slf4j.Slf4j;
import oracle.events.core.model.Event;
import oracle.events.core.repository.EventRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class EventService {

    @Autowired
    private EventRepository eventRepository;

    public Page<Event>  findAll(Specification<Event> spec, Pageable pageable){
        return (Page<Event>) eventRepository.findAll(spec, pageable);
    }

    public List<Event> findAll(){
        return (List<Event>) eventRepository.findAll();
    }

    public Optional<Event> findEvent(UUID eventId){
        return eventRepository.findById(eventId);
    }

    public Optional<Event> saveEvent(Event event){
        if(event.getAtDate() == null){
            event.setAtDate(LocalDateTime.now());
        }

        Optional<Event> toReturn = Optional.empty();

        try{
            toReturn = Optional.ofNullable(eventRepository.save(event));
        }catch (Exception e){
            log.error("DB {}", e.getMessage());
        }

        return toReturn;
    }

    public Optional<Event> updateEvent(Event event, Event founded){
        event.setEventId(founded.getEventId());

        if(StringUtils.isBlank(event.getTitle())){
            event.setTitle(founded.getTitle());
        }

        if(Objects.isNull(event.getAtDate())){
            event.setAtDate(founded.getAtDate());
        }

        if(StringUtils.isBlank(event.getDescription())){
            event.setDescription(founded.getDescription());
        }

        if(event.equals(founded)) return Optional.empty();

        return saveEvent(event);
    }

    public boolean deleteEvent(UUID eventId){
        boolean deleted = false;

        try{
            eventRepository.deleteById(eventId);
            deleted = true;
        }catch (Exception e){
            log.error("DB {}", e.getMessage());
        }

        return deleted;
    }

}
