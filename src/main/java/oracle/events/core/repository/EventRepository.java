package oracle.events.core.repository;

import oracle.events.core.model.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface EventRepository extends CrudRepository<Event, UUID> {

    Optional<Event> findById(UUID eventId);

    Page<Event> findAll(Specification<Event> spec, Pageable pageable);

}
