package oracle.events.core.exception;

public class EntityNotProcessedException extends RuntimeException {

    public EntityNotProcessedException() { super(); }

    public EntityNotProcessedException(String message) {
        super(message);
    }

    public EntityNotProcessedException(String message, Throwable cause) {
        super(message, cause);
    }

}
