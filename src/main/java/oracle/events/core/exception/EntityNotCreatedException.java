package oracle.events.core.exception;

public class EntityNotCreatedException extends RuntimeException {

    public EntityNotCreatedException() { super(); }

    public EntityNotCreatedException(String message) {
        super(message);
    }

    public EntityNotCreatedException(String message, Throwable cause) {
        super(message, cause);
    }

}
