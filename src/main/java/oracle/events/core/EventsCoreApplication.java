package oracle.events.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

@SpringBootApplication
@EntityScan(basePackageClasses = {EventsCoreApplication.class, Jsr310JpaConverters.class})
public class EventsCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(EventsCoreApplication.class, args);
    }
}
