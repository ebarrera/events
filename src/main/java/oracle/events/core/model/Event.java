package oracle.events.core.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import oracle.events.core.util.LocalDateTimeConverter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
@Table(name = "event")
@ToString
@EqualsAndHashCode
public class Event implements Serializable {

    @Id
    @Column(name = "event_id", columnDefinition = "BINARY(16)", nullable = false)
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private UUID eventId;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description")
    private String description;

    @JsonAlias({"atDate", "at_date"})
    @Column(name = "at_date")
    @Convert(converter = LocalDateTimeConverter.class)
    private LocalDateTime atDate;


}
