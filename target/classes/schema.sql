create table event (
  event_id uuid not null primary key default random_uuid,
  title varchar(100) not null,
  description varchar(255),
  at_date timestamp not null default current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=utf8;